## Boost proxy repository for hell

This is proxy repository for [boost libraries](http://www.boost.org/), which allow you to build and install them using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of boost using hell, have improvement idea, or want to request support for other versions of boost, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in boost itself please read and follow [Boost bug reporting guideline](http://www.boost.org/development/bugs.html), because here we don't do any kind of boost development.

## Important Notes

This package except hell `required targets` additionally supports `inject targets`:
 * `BOOST_MISC_FLAGS` - additional flags which should be passed to b2. Default is none aditional flags.
 * `BOOST_CUSTOM_PROJECT_CONFIG_PATH` - custom project config file (project-config.jam), useful for custom compiler support. Default is file produced by boost build system.
 * `BOOST_ADDRESS_MODEL` - Select address model `<32|64>`. Default is `32` if `CMAKE_SIZEOF_VOID_P=4`, otherwise `64`
 * `BOOST_BUILD_TYPE` - whather to build release or debug version of libraries `<debug|release>`. Default is `release` if `CMAKE_BUILD_TYPE=Release`, `debug` otherwise.
 * `BOOST_LIBRARY_LINKAGE_TYPE` - whather to build static or shared libraries `<static|shared>`. Default is `static`.
 * `BOOST_RUNTIME_LINKAGE_TYPE` - Whather to link to static or shared C and C++ runtime `<static|shared>`. Default is `static`.
 * `BOOST_THREADING_TYPE` - Whather to build single or miltithreaded binaries `<multi|single>`. Default is `multi`